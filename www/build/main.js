webpackJsonp([0],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addmoney_addmoney__ = __webpack_require__(208);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WalletPage = /** @class */ (function () {
    function WalletPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    WalletPage.prototype.addmoney = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__addmoney_addmoney__["a" /* AddmoneyPage */]);
    };
    WalletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-wallet',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\wallet\wallet.html"*/'<ion-header class="bg-theme">\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'wallet\' | translate}}</ion-title>\n    </ion-navbar>\n\n    <div class="banner" text-center>\n        <h4>{{\'available_balance\' | translate}}</h4>\n        <h1>$150.00</h1>\n        <ion-row>\n            <ion-col col-12>\n                <button ion-button block class="btn send_to_bank_btn" (click)="addmoney()">{{\'send_to_bank\' | translate}}</button>\n            </ion-col>\n        </ion-row>\n    </div>\n</ion-header>\n\n<ion-content class="bg-light">\n    <ion-list no-lines>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n        <ion-item>\n            <div class="driver_details d-flex">\n                <div class="driver_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="text_box">\n                    <h2 class="d-flex">\n                        Paid for ride\n                        <small>23rd Jan, 2019, 11:21 am </small>\n                        <span class="end">-$85.00</span>\n                    </h2>\n                    <h3>\n                        Newark Ave, Journla\n                        <span>To</span>\n                        Sip Ave, Journla\n                    </h3>\n                </div>\n            </div>\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\wallet\wallet.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], WalletPage);
    return WalletPage;
}());

//# sourceMappingURL=wallet.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MytripsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MytripsPage = /** @class */ (function () {
    function MytripsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.tab = "past";
    }
    MytripsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mytrips',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\mytrips\mytrips.html"*/'<ion-header class="bg-theme">\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'my_trips\' | translate}}</ion-title>\n    </ion-navbar>\n    <div class="bg-green">\n        <ion-segment [(ngModel)]="tab">\n            <ion-segment-button value="past" class="text-white">\n                {{\'past\' | translate}}\n            </ion-segment-button>\n            <ion-segment-button value="upcoming" class="text-white">\n                {{\'upcoming\' | translate}}\n            </ion-segment-button>\n        </ion-segment>\n    </div>\n</ion-header>\n\n<ion-content class="bg-light">\n    <div [ngSwitch]="tab">\n        <ion-list no-lines *ngSwitchCase="\'past\'">\n            <ion-item>\n                <div class="driver_details d-flex">\n                    <div class="driver_img">\n                        <img src="assets/imgs/2.png">\n                    </div>\n                    <div class="text_box">\n                        <h2 class="d-flex">\n                            Today 8:21 am\n                            <span class="end">$85.00</span>\n                        </h2>\n                        <h3 class="d-flex">\n                            Suzuki Swift Dezire\n                            <span class="end">Paid Via Wallet</span>\n                        </h3>\n                    </div>\n                </div>\n                <div class="from_to">\n                    <div class="box from d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Newark Ave, Journla Square, Nj</p>\n                    </div>\n                    <div class="box to d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Sip Ave, Journla Square, Nj</p>\n                    </div>\n                </div>\n            </ion-item>\n\n            <ion-item>\n                <div class="driver_details d-flex">\n                    <div class="driver_img">\n                        <img src="assets/imgs/2.png">\n                    </div>\n                    <div class="text_box">\n                        <h2 class="d-flex">\n                            Today 8:21 am\n                            <span class="end">$85.00</span>\n                        </h2>\n                        <h3 class="d-flex">\n                            Suzuki Swift Dezire\n                            <span class="end">Paid Via Wallet</span>\n                        </h3>\n                    </div>\n                </div>\n                <div class="from_to">\n                    <div class="box from d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Newark Ave, Journla Square, Nj</p>\n                    </div>\n                    <div class="box to d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Sip Ave, Journla Square, Nj</p>\n                    </div>\n                </div>\n            </ion-item>\n\n\n            <ion-item>\n                <div class="driver_details d-flex">\n                    <div class="driver_img">\n                        <img src="assets/imgs/2.png">\n                    </div>\n                    <div class="text_box">\n                        <h2 class="d-flex">\n                            Today 8:21 am\n                            <span class="end">$85.00</span>\n                        </h2>\n                        <h3 class="d-flex">\n                            Suzuki Swift Dezire\n                            <span class="end">Paid Via Wallet</span>\n                        </h3>\n                    </div>\n                </div>\n                <div class="from_to">\n                    <div class="box from d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Newark Ave, Journla Square, Nj</p>\n                    </div>\n                    <div class="box to d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Sip Ave, Journla Square, Nj</p>\n                    </div>\n                </div>\n            </ion-item>\n\n            <ion-item>\n                <div class="driver_details d-flex">\n                    <div class="driver_img">\n                        <img src="assets/imgs/2.png">\n                    </div>\n                    <div class="text_box">\n                        <h2 class="d-flex">\n                            Today 8:21 am\n                            <span class="end">$85.00</span>\n                        </h2>\n                        <h3 class="d-flex">\n                            Suzuki Swift Dezire\n                            <span class="end">Paid Via Wallet</span>\n                        </h3>\n                    </div>\n                </div>\n                <div class="from_to">\n                    <div class="box from d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Newark Ave, Journla Square, Nj</p>\n                    </div>\n                    <div class="box to d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Sip Ave, Journla Square, Nj</p>\n                    </div>\n                </div>\n            </ion-item>\n\n            <ion-item>\n                <div class="driver_details d-flex">\n                    <div class="driver_img">\n                        <img src="assets/imgs/2.png">\n                    </div>\n                    <div class="text_box">\n                        <h2 class="d-flex">\n                            Today 8:21 am\n                            <span class="end">$85.00</span>\n                        </h2>\n                        <h3 class="d-flex">\n                            Suzuki Swift Dezire\n                            <span class="end">Paid Via Wallet</span>\n                        </h3>\n                    </div>\n                </div>\n                <div class="from_to">\n                    <div class="box from d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Newark Ave, Journla Square, Nj</p>\n                    </div>\n                    <div class="box to d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Sip Ave, Journla Square, Nj</p>\n                    </div>\n                </div>\n            </ion-item>\n\n\n            <ion-item>\n                <div class="driver_details d-flex">\n                    <div class="driver_img">\n                        <img src="assets/imgs/2.png">\n                    </div>\n                    <div class="text_box">\n                        <h2 class="d-flex">\n                            Today 8:21 am\n                            <span class="end">$85.00</span>\n                        </h2>\n                        <h3 class="d-flex">\n                            Suzuki Swift Dezire\n                            <span class="end">Paid Via Wallet</span>\n                        </h3>\n                    </div>\n                </div>\n                <div class="from_to">\n                    <div class="box from d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Newark Ave, Journla Square, Nj</p>\n                    </div>\n                    <div class="box to d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Sip Ave, Journla Square, Nj</p>\n                    </div>\n                </div>\n            </ion-item>\n        </ion-list>\n\n        <ion-list *ngSwitchCase="\'upcoming\'">\n            <ion-item>\n                <div class="driver_details d-flex">\n                    <div class="driver_img">\n                        <img src="assets/imgs/2.png">\n                    </div>\n                    <div class="text_box">\n                        <h2 class="d-flex">\n                            Today 8:21 am\n                            <span class="end">$85.00</span>\n                        </h2>\n                        <h3 class="d-flex">\n                            Suzuki Swift Dezire\n                            <span class="end">Paid Via Wallet</span>\n                        </h3>\n                    </div>\n                </div>\n                <div class="from_to">\n                    <div class="box from d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Newark Ave, Journla Square, Nj</p>\n                    </div>\n                    <div class="box to d-flex">\n                        <ion-icon class="material-icons">fiber_manual_record</ion-icon>\n                        <p> Sip Ave, Journla Square, Nj</p>\n                    </div>\n                </div>\n            </ion-item>\n        </ion-list>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\mytrips\mytrips.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], MytripsPage);
    return MytripsPage;
}());

//# sourceMappingURL=mytrips.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcceptPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cancelride_cancelride__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__location_location__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__riderinfo_riderinfo__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AcceptPage = /** @class */ (function () {
    function AcceptPage(navCtrl, modalCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
    }
    AcceptPage.prototype.ionViewDidLoad = function () {
        var latLng = new google.maps.LatLng(20.5937, 78.9629);
        this.loadMap(latLng);
    };
    AcceptPage.prototype.loadMap = function (latLng) {
        var mapOptions = {
            center: latLng,
            zoom: 12,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    };
    //	acceptriderPage() {
    //		let modal = this.modalCtrl.create(AcceptriderPage);
    //		modal.present();
    //    }
    AcceptPage.prototype.cancelride = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__cancelride_cancelride__["a" /* CancelridePage */]);
        modal.present();
    };
    AcceptPage.prototype.riderinfo = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__riderinfo_riderinfo__["a" /* RiderinfoPage */]);
        modal.present();
    };
    AcceptPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    AcceptPage.prototype.location = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__location_location__["a" /* LocationPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["t" /* ElementRef */])
    ], AcceptPage.prototype, "mapElement", void 0);
    AcceptPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-accept',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\accept\accept.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            <small text-capitalize (click)="cancelride()">{{\'cacel_ride\' | translate}}</small>\n            <span>Sam Smith</span>\n            <small (click)="riderinfo()" class="end" text-capitalize>{{\'info\' | translate}}</small>\n        </ion-title>\n    </ion-navbar>\n    <ion-list no-lines>\n        <ion-item>\n            <ion-avatar item-start>\n                <img src="assets/imgs/2.png">\n            </ion-avatar>\n            <div class="text">\n                <h3 class="text-light">{{\'pick_up_from\' | translate}}</h3>\n                <h2>360 E 65th St, New York, NY 10065, USA</h2>\n            </div>\n            <div class="navigate" item-end>\n                <ion-icon class="material-icons">navigation</ion-icon>\n                <h3 class="text-light">{{\'navigate\' | translate}}</h3>\n            </div>\n        </ion-item>\n    </ion-list>\n</ion-header>\n\n<ion-content>\n    <div #map id="map" class="mymap"></div>\n</ion-content>\n<ion-footer no-border>\n    <button ion-button block class="shadow-green btn" (click)="location()">{{\'arrived\' | translate}}</button>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\accept\accept.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["d" /* ModalController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["g" /* ViewController */]])
    ], AcceptPage);
    return AcceptPage;
}());

//# sourceMappingURL=accept.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FarePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FarePage = /** @class */ (function () {
    function FarePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    FarePage.prototype.tabs = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    FarePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fare',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\fare\fare.html"*/'<ion-header>\n    <ion-navbar>\n        <!--\n<button ion-button menuToggle>\n</button>\n-->\n        <ion-title>{{\'receipt\' | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-light">\n    <div class="bg-white recipt" text-center>\n        <h6>{{\'fare_summary\' | translate}}</h6>\n        <h1 class="text-black">$23.00</h1>\n        <p>22 Oct, 2017, 12:30</p>\n        <ion-list>\n            <ion-item>\n                <ion-avatar item-start>\n                    <img src="assets/imgs/2.png">\n                </ion-avatar>\n                <p class="text-black">{{\'rate_now\' | translate}}</p>\n                <h2 class="text-black">Sam Smith</h2>\n            </ion-item>\n        </ion-list>\n    </div>\n\n</ion-content>\n\n<ion-footer no-border>\n    <div class="rateing_btn">\n        <div class="rateing">\n            <ion-icon name="star" class=""></ion-icon>\n            <ion-icon name="star" class=""></ion-icon>\n            <ion-icon name="star" class=""></ion-icon>\n            <ion-icon name="star" class=""></ion-icon>\n            <ion-icon name="star" class=""></ion-icon>\n        </div>\n        <button ion-button block class="btn shadow-red" (click)="tabs()">{{\'submit_rating\' | translate}}</button>\n    </div>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\fare\fare.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], FarePage);
    return FarePage;
}());

//# sourceMappingURL=fare.js.map

/***/ }),

/***/ 120:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 120;

/***/ }),

/***/ 163:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 163;

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EarningsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__wallet_wallet__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EarningsPage = /** @class */ (function () {
    function EarningsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.all_week = "week6";
    }
    EarningsPage.prototype.wallet = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__wallet_wallet__["a" /* WalletPage */]);
    };
    EarningsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-earnings',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\earnings\earnings.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-list no-lines>\n            <ion-item>\n                <p item-start class="active" text-start>{{\'offline\' | translate}}</p>\n                <ion-toggle checked="false"></ion-toggle>\n                <p item-end class="disabled" text-end> {{\'online \' | translate}}</p>\n            </ion-item>\n        </ion-list>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content class="bg-light">\n    <div class="container">\n        <ion-scroll scrollX="true">\n            <ion-segment [(ngModel)]="all_week">\n                <ion-segment-button value="week1">\n                    Mar 30 - Apr 5\n                </ion-segment-button>\n                <ion-segment-button value="week2">\n                    Mar 30 - Apr 5\n                </ion-segment-button>\n                <ion-segment-button value="week3">\n                    Mar 30 - Apr 5\n                </ion-segment-button>\n                <ion-segment-button value="week4">\n                    Mar 30 - Apr 5\n                </ion-segment-button>\n                <ion-segment-button value="week5">\n                    Mar 30 - Apr 5\n                </ion-segment-button>\n                <ion-segment-button value="week6">\n                    Mar 30 - Apr 5\n                </ion-segment-button>\n            </ion-segment>\n        </ion-scroll>\n\n        <div class="" [ngSwitch]="all_week">\n            <div class="graph_container d-flex" *ngSwitchCase="\'week6\'">\n                <div class="graph_number">\n                    <p>200</p>\n                    <p>150</p>\n                    <p>100</p>\n                    <p>50</p>\n                </div>\n                <div class="graph_box d-flex">\n                    <div class="graph">\n                        <div class="bar" style="height: 50%"></div>\n                        <p text-center>M</p>\n                        <h2 text-center>$60</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 60%"></div>\n                        <p text-center>TU</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 40%"></div>\n                        <p text-center>W</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 30%"></div>\n                        <p text-center>TH</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph active">\n                        <div class="bar" style="height: 80%"></div>\n                        <p text-center>F</p>\n                        <h2 text-center>$180</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height:0%"></div>\n                        <p text-center>Sa</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>SU</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                </div>\n            </div>\n            <div class="graph_container d-flex" *ngSwitchCase="\'week5\'">\n                <div class="graph_number">\n                    <p>200</p>\n                    <p>150</p>\n                    <p>100</p>\n                    <p>50</p>\n                </div>\n                <div class="graph_box d-flex">\n                    <div class="graph">\n                        <div class="bar" style="height: 50%"></div>\n                        <p text-center>M</p>\n                        <h2 text-center>$60</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 60%"></div>\n                        <p text-center>TU</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 40%"></div>\n                        <p text-center>W</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 30%"></div>\n                        <p text-center>TH</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph ">\n                        <div class="bar" style="height: 40%"></div>\n                        <p text-center>F</p>\n                        <h2 text-center>$180</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height:0%"></div>\n                        <p text-center>Sa</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                    <div class="graph active">\n                        <div class="bar" style="height: 1000%"></div>\n                        <p text-center>SU</p>\n                        <h2 text-center>$200</h2>\n                    </div>\n                </div>\n            </div>\n            <div class="graph_container d-flex" *ngSwitchCase="\'week4\'">\n                <div class="graph_number">\n                    <p>200</p>\n                    <p>150</p>\n                    <p>100</p>\n                    <p>50</p>\n                </div>\n                <div class="graph_box d-flex">\n                    <div class="graph">\n                        <div class="bar" style="height: 50%"></div>\n                        <p text-center>M</p>\n                        <h2 text-center>$60</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 60%"></div>\n                        <p text-center>TU</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 40%"></div>\n                        <p text-center>W</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 30%"></div>\n                        <p text-center>TH</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 80%"></div>\n                        <p text-center>F</p>\n                        <h2 text-center>$180</h2>\n                    </div>\n                    <div class="graph active">\n                        <div class="bar" style="height:100%"></div>\n                        <p text-center>Sa</p>\n                        <h2 text-center>$200</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 5%"></div>\n                        <p text-center>SU</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                </div>\n            </div>\n            <div class="graph_container d-flex" *ngSwitchCase="\'week3\'">\n                <div class="graph_number">\n                    <p>200</p>\n                    <p>150</p>\n                    <p>100</p>\n                    <p>50</p>\n                </div>\n                <div class="graph_box d-flex">\n                    <div class="graph">\n                        <div class="bar" style="height: 50%"></div>\n                        <p text-center>M</p>\n                        <h2 text-center>$60</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 60%"></div>\n                        <p text-center>TU</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 40%"></div>\n                        <p text-center>W</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 10%"></div>\n                        <p text-center>TH</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>F</p>\n                        <h2 text-center>$180</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height:0%"></div>\n                        <p text-center>Sa</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>SU</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                </div>\n            </div>\n            <div class="graph_container d-flex" *ngSwitchCase="\'week2\'">\n                <div class="graph_number">\n                    <p>200</p>\n                    <p>150</p>\n                    <p>100</p>\n                    <p>50</p>\n                </div>\n                <div class="graph_box d-flex">\n                    <div class="graph active">\n                        <div class="bar" style="height: 50%"></div>\n                        <p text-center>M</p>\n                        <h2 text-center>$50</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>TU</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>W</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>TH</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph ">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>F</p>\n                        <h2 text-center>$180</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height:0%"></div>\n                        <p text-center>Sa</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>SU</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                </div>\n            </div>\n            <div class="graph_container d-flex" *ngSwitchCase="\'week1\'">\n                <div class="graph_number">\n                    <p>200</p>\n                    <p>150</p>\n                    <p>100</p>\n                    <p>50</p>\n                </div>\n                <div class="graph_box d-flex">\n                    <div class="graph">\n                        <div class="bar" style="height: 50%"></div>\n                        <p text-center>M</p>\n                        <h2 text-center>$60</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 60%"></div>\n                        <p text-center>TU</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 40%"></div>\n                        <p text-center>W</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 30%"></div>\n                        <p text-center>TH</p>\n                        <h2 text-center>$70</h2>\n                    </div>\n                    <div class="graph active">\n                        <div class="bar" style="height: 80%"></div>\n                        <p text-center>F</p>\n                        <h2 text-center>$180</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height:0%"></div>\n                        <p text-center>Sa</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                    <div class="graph">\n                        <div class="bar" style="height: 0%"></div>\n                        <p text-center>SU</p>\n                        <h2 text-center>0</h2>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n    <ion-card class="summery">\n        <ion-card-header>\n            <h2>{{\'earning_summary\' | translate}}</h2>\n        </ion-card-header>\n        <ion-card-content>\n            <ion-row>\n                <ion-col col-4>\n                    <h4>{{\'total_earnings\' | translate}}</h4>\n                    <h2 class="d-flex">\n                        <ion-icon class="material-icons">account_balance_wallet</ion-icon>\n                        $ 351\n                    </h2>\n                </ion-col>\n                <ion-col col-4>\n                    <h4>{{\'in_cash\' | translate}}</h4>\n                    <h2 class="d-flex">\n                        <ion-icon class="material-icons">account_balance_wallet</ion-icon>\n                        $ 125\n                    </h2>\n                </ion-col>\n                <ion-col col-4>\n                    <button ion-button icon-end block class="btn" (click)="wallet()">{{\'wallet\' | translate}}\n                        <ion-icon class="material-icons">keyboard_arrow_right</ion-icon>\n                    </button>\n                </ion-col>\n            </ion-row>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\earnings\earnings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], EarningsPage);
    return EarningsPage;
}());

//# sourceMappingURL=earnings.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddmoneyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AddmoneyPage = /** @class */ (function () {
    function AddmoneyPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AddmoneyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-addmoney',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\addmoney\addmoney.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'send_to_bank\' | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="banner bg-light" text-center>\n        <h4>{{\'available_balance\' | translate}}</h4>\n        <h1>$150.00</h1>\n    </div>\n    <ion-list no-lines class="form">\n        <h2>{{\'enter_bank_info\' | translate}}</h2>\n        <div class="form_container">\n            <ion-item class="bg-light">\n                <ion-label>{{\'account_number\' | translate}}</ion-label>\n                <ion-input type="text" value="1122 3344 5566 7788" text-end></ion-input>\n            </ion-item>\n            <ion-item class="bg-light">\n                <ion-label>{{\'account_holder_name\' | translate}}</ion-label>\n                <ion-input type="text" value="Sam Smith" text-end></ion-input>\n            </ion-item>\n            <ion-item class="bg-light">\n                <ion-label>{{\'bank_code\' | translate}}</ion-label>\n                <ion-input type="text" value="XYZBANK001" text-end></ion-input>\n            </ion-item>\n        </div>\n    </ion-list>\n\n    <ion-list no-lines class="form">\n        <h2>{{\'enter_amount_to_transfer\' | translate}}</h2>\n        <div class="form_container">\n            <ion-item class="bg-light">\n                <ion-label>{{\'enter_amount_to_transfer_in\' | translate}}</ion-label>\n                <ion-input type="text" value="$100" text-end></ion-input>\n            </ion-item>\n        </div>\n    </ion-list>\n</ion-content>\n<ion-footer no-border>\n    <button ion-button block class="btn">{{\'proceed\' | translate}}</button>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\addmoney\addmoney.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], AddmoneyPage);
    return AddmoneyPage;
}());

//# sourceMappingURL=addmoney.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RatingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mytrips_mytrips__ = __webpack_require__(106);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RatingsPage = /** @class */ (function () {
    function RatingsPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    RatingsPage.prototype.mytrips = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__mytrips_mytrips__["a" /* MytripsPage */]);
    };
    RatingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-ratings',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\ratings\ratings.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-list no-lines>\n            <ion-item>\n                <p item-start class="active" text-start>{{\'offline\' | translate}}</p>\n                <ion-toggle checked="false"></ion-toggle>\n                <p item-end class="disabled" text-end> {{\'online \' | translate}}</p>\n            </ion-item>\n        </ion-list>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-light">\n    <div class="rating_continer">\n        <p text-center class="title-head">{{\'your_current_rating\' | translate}}</p>\n        <h1 text-center>\n            <ion-icon name="md-star"></ion-icon>\n            4.68\n        </h1>\n        <div class="rating_box">\n            <div class="rating d-flex active">\n                <p class="rating-num d-flex ">5 <ion-icon name="md-star" class="end"></ion-icon>\n                </p>\n                <div class="rating_scale">\n                    <div class="rating_scale_active" style="width: 90%;">\n                    </div>\n                </div>\n                <p>392</p>\n            </div>\n            <div class="rating d-flex">\n                <p class="rating-num d-flex">4 <ion-icon name="md-star" class="end"></ion-icon>\n                </p>\n                <div class="rating_scale">\n                    <div class="rating_scale_active" style="width: 60%;">\n                    </div>\n                </div>\n                <p>189</p>\n            </div>\n            <div class="rating d-flex">\n                <p class="rating-num d-flex">3 <ion-icon name="md-star" class="end"></ion-icon>\n                </p>\n                <div class="rating_scale">\n                    <div class="rating_scale_active" style="width: 40%;">\n                    </div>\n                </div>\n                <p>34</p>\n            </div>\n            <div class="rating d-flex">\n                <p class="rating-num d-flex">2 <ion-icon name="md-star" class="end"></ion-icon>\n                </p>\n                <div class="rating_scale">\n                    <div class="rating_scale_active" style="width: 30%;">\n                    </div>\n                </div>\n                <p>50</p>\n            </div>\n            <div class="rating d-flex">\n                <p class="rating-num d-flex">1 <ion-icon name="md-star" class="end"></ion-icon>\n                </p>\n                <div class="rating_scale">\n                    <div class="rating_scale_active" style="width: 10%;">\n                    </div>\n                </div>\n                <p>10</p>\n            </div>\n        </div>\n    </div>\n\n\n    <ion-card class="summery">\n        <ion-card-header>\n            <h2>{{\'trip_summary\' | translate}}</h2>\n        </ion-card-header>\n        <ion-card-content>\n            <ion-row>\n                <ion-col col-5>\n                    <h4>{{\'total_trips_yet\' | translate}}</h4>\n                    <h2 class="d-flex">\n                        <ion-icon class="material-icons">directions_car</ion-icon>\n                        870\n                    </h2>\n                </ion-col>\n                <ion-col col-7>\n                    <button ion-button icon-end block class="btn" (click)="mytrips()">{{\'view_all_trips\' | translate}}\n                        <ion-icon class="material-icons">keyboard_arrow_right</ion-icon>\n                    </button>\n                </ion-col>\n            </ion-row>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\ratings\ratings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], RatingsPage);
    return RatingsPage;
}());

//# sourceMappingURL=ratings.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accept_accept__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.checked = false;
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var latLng = new google.maps.LatLng(20.5937, 78.9629);
        this.loadMap(latLng);
    };
    HomePage.prototype.loadMap = function (latLng) {
        var mapOptions = {
            center: latLng,
            zoom: 4,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    };
    HomePage.prototype.accept = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__accept_accept__["a" /* AcceptPage */]);
        modal.present();
    };
    HomePage.prototype.onlineOfflineToggle = function () {
        console.log('checked ' + this.checked);
    };
    HomePage.prototype.declinePage = function () {
        //this.navCtrl.push(AcceptPage);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "mapElement", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\home\home.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-list no-lines>\n            <ion-item>\n                <p item-start class="active" text-start>{{\'offline\' | translate}}</p>\n                <ion-toggle [(ngModel)]="checked" (ionChange)="onlineOfflineToggle()" checked="false"></ion-toggle>\n                <p item-end class="disabled" text-end> {{\'online \' | translate}}</p>\n            </ion-item>\n        </ion-list>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div #map id="map" class="mymap"></div>\n    <!-- <p (click)="locationPage()" class="btn bg-green text-white" text-center padding>Accept rider</p> -->\n</ion-content>\n<ion-footer no-border>\n    <ion-card *ngIf="!checked" class="summery shadow-black ">\n        <ion-card-header>\n            <h2>{{\'today_summery\' | translate}}</h2>\n        </ion-card-header>\n        <ion-card-content>\n            <ion-row>\n                <ion-col col-6>\n                    <h4>{{\'total_trips_today\' | translate}}</h4>\n                    <h2 class="d-flex">\n                        <ion-icon class="material-icons">directions_car</ion-icon>\n                        9 Trips\n                    </h2>\n                </ion-col>\n                <ion-col col-6>\n                    <h4>{{\'today_earning\' | translate}}</h4>\n                    <h2 class="d-flex">\n                        <ion-icon class="material-icons">account_balance_wallet</ion-icon>\n                        140.20 $\n                    </h2>\n                </ion-col>\n            </ion-row>\n        </ion-card-content>\n    </ion-card>\n    <ion-card class="passanger_info" *ngIf="checked">\n        <ion-card-header>\n            <h2 class="d-flex">{{\'passanger_info\' | translate}} <span class="end" (click)="declinePage()">{{\'decline\' | translate}}</span></h2>\n        </ion-card-header>\n        <ion-card-content>\n            <div class="passanger-info d-flex">\n                <div class="passanger_img">\n                    <img src="assets/imgs/2.png">\n                </div>\n                <div class="passanger_details">\n                    <h2>\n                        Sam Smith\n                    </h2>\n                    <p class="text-black d-flex">\n                        <small>4.7</small>\n                        <span class="rateing  d-flex">\n                            <ion-icon class="active" name="ios-star"></ion-icon>\n                            <ion-icon class="active" name="ios-star"></ion-icon>\n                            <ion-icon class="active" name="ios-star"></ion-icon>\n                            <ion-icon class="active" name="ios-star"></ion-icon>\n                            <ion-icon class="" name="ios-star"></ion-icon>\n                        </span>\n                    </p>\n                </div>\n            </div>\n            <ion-row>\n                <ion-col col-6>\n                    <h4>{{\'pick_up_from\' | translate}}</h4>\n                    <h2>Summit Avenue</h2>\n                </ion-col>\n                <ion-col col-6>\n                    <h4>{{\'how_far\' | translate}}</h4>\n                    <h2> 0.5 km away</h2>\n                </ion-col>\n            </ion-row>\n            <button ion-button block class="btn" (click)="accept()">{{\'accept\' | translate}}</button>\n        </ion-card-content>\n    </ion-card>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["d" /* ModalController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 211:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CancelridePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CancelridePage = /** @class */ (function () {
    function CancelridePage(navCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
    }
    CancelridePage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    CancelridePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cancelride',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\cancelride\cancelride.html"*/'<ion-content (click)="dismiss()">\n</ion-content>\n<ion-footer no-border (click)="dismiss()">\n    <ion-list no-lines>\n        <h2>{{\'why_are_you_cancling\' | translate}}</h2>\n        <ion-item>\n            <h3>{{\'rider_no_show\' | translate}}</h3>\n        </ion-item>\n        <ion-item>\n            <h3>{{\'rider_requested_cancel\' | translate}}</h3>\n        </ion-item>\n        <ion-item>\n            <h3>{{\'wrong_address_show\' | translate}}</h3>\n        </ion-item>\n        <ion-item>\n            <h3>{{\'other\' | translate}}</h3>\n        </ion-item>\n    </ion-list>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\cancelride\cancelride.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */]])
    ], CancelridePage);
    return CancelridePage;
}());

//# sourceMappingURL=cancelride.js.map

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__riderinfo_riderinfo__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trip_end_trip_end__ = __webpack_require__(213);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LocationPage = /** @class */ (function () {
    function LocationPage(navCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
    }
    LocationPage.prototype.ionViewDidLoad = function () {
        var latLng = new google.maps.LatLng(20.5937, 78.9629);
        this.loadMap(latLng);
    };
    LocationPage.prototype.loadMap = function (latLng) {
        var mapOptions = {
            center: latLng,
            zoom: 12,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    };
    LocationPage.prototype.trip_end = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__trip_end_trip_end__["a" /* Trip_endPage */]);
    };
    LocationPage.prototype.riderinfo = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__riderinfo_riderinfo__["a" /* RiderinfoPage */]);
        modal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["t" /* ElementRef */])
    ], LocationPage.prototype, "mapElement", void 0);
    LocationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-location',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\location\location.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            <span>Sam Smith</span>\n            <small (click)="riderinfo()" class="end" text-capitalize>{{\'info\' | translate}}</small>\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div #map id="map" class="mymap"></div>\n    <ion-list class="form" no-lines>\n        <ion-item class="from">\n            <ion-icon class="material-icons" item-start>fiber_manual_record</ion-icon>\n            <p class="d-flex"><span class="">{{\'from\' | translate}}</span> Newark Ave, Journla Square, Nj</p>\n        </ion-item>\n        <ion-item class="to">\n            <ion-icon class="material-icons" item-start>fiber_manual_record</ion-icon>\n            <p class="d-flex"><span class="">{{\'to\' | translate}}</span>Sip Ave, Journla Square, Nj</p>\n        </ion-item>\n    </ion-list>\n</ion-content>\n<ion-footer no-border>\n    <button ion-button block class="shadow-green btn" (click)="trip_end()">{{\'double_tap_to_begin_trip_ming\' | translate}}</button>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\location\location.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["d" /* ModalController */]])
    ], LocationPage);
    return LocationPage;
}());

//# sourceMappingURL=location.js.map

/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Trip_endPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fare_fare__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__riderinfo_riderinfo__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Trip_endPage = /** @class */ (function () {
    function Trip_endPage(navCtrl, modalCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
    }
    Trip_endPage.prototype.ionViewDidLoad = function () {
        var latLng = new google.maps.LatLng(20.5937, 78.9629);
        this.loadMap(latLng);
    };
    Trip_endPage.prototype.loadMap = function (latLng) {
        var mapOptions = {
            center: latLng,
            zoom: 12,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    };
    //	acceptriderPage() {
    //		let modal = this.modalCtrl.create(AcceptriderPage);
    //		modal.present();
    //    }
    Trip_endPage.prototype.riderinfo = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_3__riderinfo_riderinfo__["a" /* RiderinfoPage */]);
        modal.present();
    };
    Trip_endPage.prototype.fare = function () {
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_2__fare_fare__["a" /* FarePage */]);
        modal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], Trip_endPage.prototype, "mapElement", void 0);
    Trip_endPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-trip_end',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\trip_end\trip_end.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            <span>Sam Smith</span>\n            <small (click)="riderinfo()" class="end" text-capitalize>{{\'info\' | translate}}</small>\n        </ion-title>\n    </ion-navbar>\n    <ion-list no-lines>\n        <ion-item>\n            <ion-avatar item-start>\n                <img src="assets/imgs/2.png">\n            </ion-avatar>\n            <div class="text">\n                <h3 class="text-light">{{\'drive_to\' | translate}}</h3>\n                <h2>360 E 65th St, New York, NY 10065, USA</h2>\n            </div>\n            <div class="navigate" item-end>\n                <ion-icon class="material-icons">navigation</ion-icon>\n                <h3 class="text-light">{{\'navigate\' | translate}}</h3>\n            </div>\n        </ion-item>\n    </ion-list>\n</ion-header>\n\n<ion-content>\n    <div #map id="map" class="mymap"></div>\n</ion-content>\n<ion-footer no-border>\n    <button ion-button block class="shadow-red btn" (click)="fare()">{{\'double_tap_to_end_trip\' | translate}}</button>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\trip_end\trip_end.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */]])
    ], Trip_endPage);
    return Trip_endPage;
}());

//# sourceMappingURL=trip_end.js.map

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__about_about__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__help_help__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__documents_documents__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__wallet_wallet__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__mytrips_mytrips__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__myprofile_myprofile__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







//import { SharePage } from '../share/share';

var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ProfilePage.prototype.aboutPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__about_about__["a" /* AboutPage */]);
    };
    ProfilePage.prototype.helpPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__help_help__["a" /* HelpPage */]);
    };
    ProfilePage.prototype.documentsPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__documents_documents__["a" /* DocumentsPage */]);
    };
    ProfilePage.prototype.wallet = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__wallet_wallet__["a" /* WalletPage */]);
    };
    ProfilePage.prototype.mytripsPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__mytrips_mytrips__["a" /* MytripsPage */]);
    };
    //    sharePage(){
    //    this.navCtrl.push(SharePage);
    //    }
    ProfilePage.prototype.myprofilePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__myprofile_myprofile__["a" /* MyprofilePage */]);
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\profile\profile.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-list no-lines>\n            <ion-item>\n                <p item-start class="active" text-start>{{\'offline\' | translate}}</p>\n                <ion-toggle checked="false"></ion-toggle>\n                <p item-end class="disabled" text-end> {{\'online \' | translate}}</p>\n            </ion-item>\n        </ion-list>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-light">\n    <ion-list no-lines>\n        <ion-item (click)="myprofilePage()">\n            <ion-avatar item-start>\n                <img src="assets/imgs/2.png">\n            </ion-avatar>\n            <h2 class="d-flex">George Smith\n                <span class="end text-theme">{{\'view_profile\' | translate}}</span>\n            </h2>\n            <p class="d-flex">\n                <small>4.7</small>\n                <span class="rateing  d-flex">\n                    <ion-icon class="active" name="ios-star"></ion-icon>\n                    <ion-icon class="active" name="ios-star"></ion-icon>\n                    <ion-icon class="active" name="ios-star"></ion-icon>\n                    <ion-icon class="active" name="ios-star"></ion-icon>\n                    <ion-icon class="" name="ios-star"></ion-icon>\n                </span>\n            </p>\n        </ion-item>\n        <ion-item class="car-detail">\n            <p class="text-">Hyundai WagonR</p>\n            <h2 class="text-dark"><strong>DL ZA 5887</strong></h2>\n        </ion-item>\n    </ion-list>\n    <div class="menu-items">\n        <ion-row>\n            <ion-col col-6>\n                <div class="manu_box" text-center (click)="mytripsPage()">\n                    <ion-icon class="material-icons">directions_car</ion-icon>\n                    <h4>{{\'my_trips\' | translate}}</h4>\n                </div>\n            </ion-col>\n            <ion-col col-6>\n                <div class="manu_box" text-center (click)="wallet()">\n                    <h2>$ 150.50</h2>\n                    <h4>{{\'wallet\' | translate}}</h4>\n                </div>\n            </ion-col>\n            <ion-col col-6>\n                <div class="manu_box" text-center (click)="helpPage()">\n                    <ion-icon class="material-icons">help</ion-icon>\n                    <h4>{{\'help\' | translate}}</h4>\n                </div>\n            </ion-col>\n            <ion-col col-6>\n                <div class="manu_box" text-center (click)="documentsPage()">\n                    <ion-icon class="material-icons">assignment_ind</ion-icon>\n                    <h4>{{\'documents\' | translate}}</h4>\n                </div>\n            </ion-col>\n            <ion-col col-6>\n                <div class="manu_box" text-center>\n                    <ion-icon class="material-icons">share</ion-icon>\n                    <h4>{{\'share\' | translate}}</h4>\n                </div>\n            </ion-col>\n            <ion-col col-6>\n                <div class="manu_box" text-center (click)="aboutPage()">\n                    <ion-icon class="material-icons">assignment</ion-icon>\n                    <h4>{{\'t&c\' | translate}}</h4>\n                </div>\n            </ion-col>\n        </ion-row>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\profile\profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\about\about.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            {{\'terms_conditions\' | translate}}\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="logo">\n        <img src="assets/imgs/logo.png">\n        <h2>{{\'cabber_3.2\' | translate}}</h2>\n    </div>\n    <div class="text_box">\n        <p>\n            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,\n        </p>\n        <p>\n            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,\n        </p>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\about\about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HelpPage = /** @class */ (function () {
    function HelpPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HelpPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-help',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\help\help.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'help\' | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-light">\n    <ion-list no-lines>\n        <h2>{{\'choose_your_issue\' | translate}}</h2>\n        <ion-item>\n            <h3 class="d-flex">{{\'trips_and_fare\' | translate}}\n                <ion-icon name="ios-arrow-down-outline" class="end"></ion-icon>\n            </h3>\n            <p>{{\'any_issue_regarding_your_trip_and_fare\' | translate}}\n            </p>\n        </ion-item>\n        <ion-item>\n            <h3 class="d-flex">{{\'Payment\' | translate}}\n                <ion-icon name="ios-arrow-down-outline" class="end"></ion-icon>\n            </h3>\n            <p>{{\'problem_while_paying_fare_or_related_issue\' | translate}}\n            </p>\n        </ion-item>\n        <ion-item>\n            <h3 class="d-flex">{{\'app_usability\' | translate}}\n\n                <ion-icon name="ios-arrow-down-outline" class="end"></ion-icon>\n            </h3>\n            <p>{{\'any_issue_while_using_our_app\' | translate}}\n            </p>\n        </ion-item>\n        <ion-item>\n            <h3 class="d-flex">{{\'account\' | translate}}\n                <ion-icon name="ios-arrow-down-outline" class="end"></ion-icon>\n            </h3>\n\n            <p>{{\'your_account_info_can_t_change_details_or_change_password\' | translate}}\n            </p>\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\help\help.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HelpPage);
    return HelpPage;
}());

//# sourceMappingURL=help.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DocumentsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DocumentsPage = /** @class */ (function () {
    function DocumentsPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    DocumentsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-documents',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\documents\documents.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            {{\'documents\' | translate}}\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-light">\n    <ion-list no-lines>\n        <ion-item class="active">\n            <h2>{{\'drier_license\' | translate}}</h2>\n            <h3>{{\'verified\' | translate}}</h3>\n            <ion-icon class="material-icons" item-end>check_circle</ion-icon>\n        </ion-item>\n\n        <ion-item>\n            <h2>{{\'goverment_id\' | translate}}</h2>\n            <h3>{{\'not_verified_yet\' | translate}}</h3>\n            <ion-icon class="material-icons" item-end>check_circle</ion-icon>\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\documents\documents.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], DocumentsPage);
    return DocumentsPage;
}());

//# sourceMappingURL=documents.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyprofilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MyprofilePage = /** @class */ (function () {
    function MyprofilePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    MyprofilePage.prototype.login = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    MyprofilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-myprofile',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\myprofile\myprofile.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n        </button>\n        <ion-title>\n            {{\'profile\' | translate}}\n            <span (click)="login()" class="end">{{\'logout\' | translate}}</span>\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="banner">\n        <div class="banner_img">\n            <img src="assets/imgs/profile.png">\n        </div>\n        <!--        <ion-icon name="md-create"></ion-icon>-->\n    </div>\n    <ion-list class="form" no-lines>\n        <ion-item>\n            <ion-label floating>{{\'first_name\' | translate}}</ion-label>\n            <ion-input type="text" value="Sam"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label floating>{{\'last_name\' | translate}}</ion-label>\n            <ion-input type="text" value="Smith"></ion-input>\n        </ion-item>\n        <div class="d-flex">\n            <ion-item>\n                <ion-label floating>{{\'contact_number\' | translate}}</ion-label>\n                <ion-input type="text" value="+1 987 654 3210"></ion-input>\n            </ion-item>\n        </div>\n        <div class="d-flex">\n            <ion-item>\n                <ion-label floating>{{\'email_address\' | translate}}</ion-label>\n                <ion-input type="text" value="appuser@email.com"></ion-input>\n            </ion-item>\n        </div>\n        <!--\n        <ion-item>\n            <ion-label floating>{{\'password\' | translate}}</ion-label>\n            <ion-input class="Password" type="text" value="* * * * * *"></ion-input>\n        </ion-item>\n-->\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\myprofile\myprofile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], MyprofilePage);
    return MyprofilePage;
}());

//# sourceMappingURL=myprofile.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__verification_verification__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SignupPage.prototype.verification = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__verification_verification__["a" /* VerificationPage */]);
    };
    SignupPage.prototype.loginPage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\signup\signup.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'sign_up\' | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding-left padding-right>\n    <img src="assets/imgs/logo.png" class="logo">\n    <div class="active-stage" padding>\n        <ion-row text-center>\n            <ion-col class="active"><span><strong></strong></span></ion-col>\n            <ion-col><span><strong></strong></span></ion-col>\n        </ion-row>\n    </div>\n    <div padding-left padding-right>\n        <ion-row class="name_camra">\n            <ion-col col-4 text-center>\n                <div class="bg-light form camra">\n                    <ion-icon name="camera"></ion-icon>\n                </div>\n            </ion-col>\n            <ion-col col-8>\n                <ion-list class="form">\n                    <ion-item class="bg-light">\n                        <ion-label>{{\'first_name\' | translate}}</ion-label>\n                        <ion-input type="text" value="Sam" text-right></ion-input>\n                    </ion-item>\n                    <ion-item class="bg-light">\n                        <ion-label>{{\'last_name\' | translate}}</ion-label>\n                        <ion-input type="text" value="Smith" text-right></ion-input>\n                    </ion-item>\n                </ion-list>\n            </ion-col>\n\n        </ion-row>\n        <ion-list class="form">\n            <ion-item class="bg-light">\n                <ion-label>{{\'create_password\' | translate}}</ion-label>\n                <ion-input class="password" type="password" value="*******" text-right></ion-input>\n            </ion-item>\n            <ion-item class="bg-light">\n                <ion-label>{{\'confirm_password\' | translate}}</ion-label>\n                <ion-input class="password" type="password" value="*******" text-right></ion-input>\n            </ion-item>\n            <ion-item class="bg-light">\n                <ion-label>{{\'phone_number\' | translate}}</ion-label>\n                <ion-input type="text" value="+91 9876543210" text-right></ion-input>\n            </ion-item>\n        </ion-list>\n        <button ion-button block class="btn round shadow-green" (click)="verification()">{{\'sign_up_now\' | translate}}</button>\n\n        <ion-row padding-left padding-right padding-top text-center class="text">\n            <ion-col (click)="loginPage()">{{\'already_registered\' | translate}} <strong class="text-theme">{{\'sign_in\' | translate}}</strong></ion-col>\n        </ion-row>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\signup\signup.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VerificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(54);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var VerificationPage = /** @class */ (function () {
    function VerificationPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    VerificationPage.prototype.tabs = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    VerificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-verification',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\verification\verification.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'verification\' | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <div padding-left padding-right>\n        <img src="assets/imgs/logo.png" class="logo">\n        <div class="active-stage" padding>\n            <ion-row text-center>\n                <ion-col class="active"><span><strong></strong></span></ion-col>\n                <ion-col class="active"><span><strong></strong></span></ion-col>\n            </ion-row>\n        </div>\n        <br>\n        <p text-center padding>{{\'enter_confirmation_code\' | translate}}<br>{{\'sent_to_you_via_sms\' | translate}}</p>\n        <div padding-top>\n            <ion-list class="form">\n                <ion-item class="bg-light">\n                    <ion-input type="text" value="6543210" text-center></ion-input>\n                </ion-item>\n            </ion-list>\n            <button ion-button block class="bg-green text-white btn round shadow-green" (click)="tabs()">{{\'next\' | translate}}</button>\n        </div>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\verification\verification.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], VerificationPage);
    return VerificationPage;
}());

//# sourceMappingURL=verification.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(242);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(283);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(300);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_http_loader__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_login_login__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_about_about__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_home_home__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_request_request__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_acceptrider_acceptrider__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_accept_accept__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_roder_roder__ = __webpack_require__(309);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_cancelride_cancelride__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_nevigate_nevigate__ = __webpack_require__(310);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_location_location__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_fare_fare__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_ratings_ratings__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_earnings_earnings__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_profile_profile__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_help_help__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_documents_documents__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_settings_settings__ = __webpack_require__(311);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_mytrips_mytrips__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_share_share__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_myprofile_myprofile__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_signup_signup__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_verification_verification__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_addmoney_addmoney__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_trip_end_trip_end__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_wallet_wallet__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_riderinfo_riderinfo__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




































function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_8__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_request_request__["a" /* RequestPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_acceptrider_acceptrider__["a" /* AcceptriderPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_roder_roder__["a" /* RoderPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_cancelride_cancelride__["a" /* CancelridePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_nevigate_nevigate__["a" /* NevigatePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_location_location__["a" /* LocationPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_fare_fare__["a" /* FarePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_ratings_ratings__["a" /* RatingsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_earnings_earnings__["a" /* EarningsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_documents_documents__["a" /* DocumentsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_mytrips_mytrips__["a" /* MytripsPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_share_share__["a" /* SharePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_myprofile_myprofile__["a" /* MyprofilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_accept_accept__["a" /* AcceptPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_verification_verification__["a" /* VerificationPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_riderinfo_riderinfo__["a" /* RiderinfoPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_trip_end_trip_end__["a" /* Trip_endPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_addmoney_addmoney__["a" /* AddmoneyPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_wallet_wallet__["a" /* WalletPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: createTranslateLoader,
                        deps: [__WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */]]
                    }
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_10__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_request_request__["a" /* RequestPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_acceptrider_acceptrider__["a" /* AcceptriderPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_roder_roder__["a" /* RoderPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_cancelride_cancelride__["a" /* CancelridePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_nevigate_nevigate__["a" /* NevigatePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_location_location__["a" /* LocationPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_fare_fare__["a" /* FarePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_ratings_ratings__["a" /* RatingsPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_earnings_earnings__["a" /* EarningsPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_help_help__["a" /* HelpPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_documents_documents__["a" /* DocumentsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_settings_settings__["a" /* SettingsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_mytrips_mytrips__["a" /* MytripsPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_share_share__["a" /* SharePage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_myprofile_myprofile__["a" /* MyprofilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_accept_accept__["a" /* AcceptPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_verification_verification__["a" /* VerificationPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_riderinfo_riderinfo__["a" /* RiderinfoPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_trip_end_trip_end__["a" /* Trip_endPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_addmoney_addmoney__["a" /* AddmoneyPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_wallet_wallet__["a" /* WalletPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 283:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__ = __webpack_require__(221);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, translate) {
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.translate = translate;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.translate.setDefaultLang('en');
            _this.translate.use('en');
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5__node_modules_ngx_translate_core__["c" /* TranslateService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__accept_accept__ = __webpack_require__(107);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RequestPage = /** @class */ (function () {
    function RequestPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    RequestPage.prototype.acceptPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__accept_accept__["a" /* AcceptPage */]);
    };
    RequestPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-request',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\request\request.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding class="bg-img">\n    <div class="card-box">\n        <ion-card class="text-black">\n            <ion-card-header>\n                <small float-left class="text-light">Passanger Info</small>\n                <small float-right class="text-green">Decline</small>\n            </ion-card-header>\n            <ion-card-content>\n                <div class="passanger-info heading">\n                    <ion-list>\n                        <ion-item>\n                            <ion-avatar item-start>\n                                <img src="assets/imgs/profile_pix.png">\n                            </ion-avatar>\n                            <h2 class="text-black">\n                                Tarun Kumar\n                            </h2>\n                            <p class="text-black">4.7\n                                <span class="rateing">\n                                <ion-icon name="star" class="text-green"></ion-icon>\n                                <ion-icon name="star" class="text-green"></ion-icon>\n                                <ion-icon name="star" class="text-green"></ion-icon>\n                                <ion-icon name="star" class="text-green"></ion-icon>\n                                <ion-icon name="star" class="text-green"></ion-icon>\n                                </span>\n                            </p>\n                        </ion-item>\n                    </ion-list>\n                </div>\n                <ion-row padding class="car-details">\n                    <ion-col>\n                        <small class="text-light">Pick up From</small>\n                        <strong class="text-black">Summit Avenue</strong>\n                    </ion-col>\n                    <ion-col>\n                        <small class="text-light">How far</small>\n                        <strong class="text-black">0.5km away</strong>\n                    </ion-col>\n                </ion-row>\n                <div text-center padding-left padding-right>\n                    <button ion-button full class="bg-green full" (click)="acceptPage()"> ACCEPT <span class="time">(02:29 Min)</span></button>\n                </div>\n            </ion-card-content>\n        </ion-card>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\request\request.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], RequestPage);
    return RequestPage;
}());

//# sourceMappingURL=request.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcceptriderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__fare_fare__ = __webpack_require__(108);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AcceptriderPage = /** @class */ (function () {
    function AcceptriderPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AcceptriderPage.prototype.farePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_1__fare_fare__["a" /* FarePage */]);
    };
    AcceptriderPage.prototype.ionViewDidLoad = function () {
        var latLng = new google.maps.LatLng(20.5937, 78.9629);
        this.loadMap(latLng);
    };
    AcceptriderPage.prototype.loadMap = function (latLng) {
        var mapOptions = {
            center: latLng,
            zoom: 12,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__angular_core__["t" /* ElementRef */])
    ], AcceptriderPage.prototype, "mapElement", void 0);
    AcceptriderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["m" /* Component */])({
            selector: 'page-acceptrider',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\acceptrider\acceptrider.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            TARUN KUMAR\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n	<div #map id="map" class="mymap"></div>\n    <ion-card>\n        <ion-card-content class="text-light">\n            <div class="rider-info heading">\n                <ion-row>\n                    <ion-col col-9>\n                        <ion-list>\n                            <ion-item>\n                                <ion-avatar item-start>\n                                    <img src="assets/imgs/profile_pix.png">\n                                </ion-avatar>\n                                <small class="text-light">Drive to</small>\n                                <h2 class="text-black">\n                                    99 Fore St, Kingsbridge TQ71AB, UK\n                                </h2>\n                            </ion-item>\n                        </ion-list>\n                    </ion-col>\n                    <ion-col text-center col-3>\n                        <ion-icon name="navigate" ios="md-navigate" class="text-green"></ion-icon>\n                        <br>\n                        <small>NEVIGATE</small>\n                    </ion-col>\n                </ion-row>\n            </div>\n        </ion-card-content>\n    </ion-card>\n    <div text-center padding-left padding-right class="btn-fix">\n        <div padding-left padding-right>\n            <button ion-button full class="bg-red btn full shadow-red" (click)="farePage()">DOUBLE TAP END TRIP</button>\n        </div>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\acceptrider\acceptrider.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["e" /* NavController */]])
    ], AcceptriderPage);
    return AcceptriderPage;
}());

//# sourceMappingURL=acceptrider.js.map

/***/ }),

/***/ 309:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RoderPage = /** @class */ (function () {
    function RoderPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    RoderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-roder',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\roder\roder.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            Contact\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-list>\n        <ion-list-header>Roder</ion-list-header>\n        <ion-item>\n            <ion-icon name="ionic" item-start></ion-icon>\n            @ionicframework\n        </ion-item>\n    </ion-list>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\roder\roder.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], RoderPage);
    return RoderPage;
}());

//# sourceMappingURL=roder.js.map

/***/ }),

/***/ 310:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NevigatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NevigatePage = /** @class */ (function () {
    function NevigatePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    NevigatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nevigate',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\nevigate\nevigate.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title text-center>\n            <small float-left text-left style="margin-right: auto;">Overview</small>\n            <span style="margin: auto;" (click)="showInfo()">TARUN KUMAR</span>\n            <small float-right text-rignt style="margin-left: auto;" (click)="showInfo()">Info</small>\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-img">\n    <ion-card>\n        <ion-card-content class="text-light">\n            <ion-row>\n                <ion-col col-9>\n                    <ion-list padding-left>\n                        <ion-item>\n                            <h2 class="text-black">\n                                Staight to Manhattan Ave\n                            </h2>\n                        </ion-item>\n                    </ion-list>\n                </ion-col>\n                <ion-col text-center col-3>\n                    <ion-icon name="navigate" ios="md-navigate" class="text-green"></ion-icon>\n                    <br>\n                    <small class=text-green>420 ft</small>\n                </ion-col>\n            </ion-row>\n        </ion-card-content>\n    </ion-card>\n    <div text-center padding-left padding-right class="btn-fix">\n        <div padding-left padding-right>\n            <button ion-button full class="bg-green full shadow-green btn" (click)="acceptriderPage()">ARRIVED</button>\n        </div>\n    </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\nevigate\nevigate.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], NevigatePage);
    return NevigatePage;
}());

//# sourceMappingURL=nevigate.js.map

/***/ }),

/***/ 311:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SettingsPage = /** @class */ (function () {
    function SettingsPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SettingsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-settings',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\settings\settings.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            SETTINGS\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-light">\n    <p padding class="text-light"><span padding-left padding-right>Map Setting</span></p>\n    <ion-card padding>\n        <ion-card-content>\n            <h2>Navigation Provider</h2>\n            <p>Google Map</p>\n        </ion-card-content>\n    </ion-card>\n    <ion-card padding>\n        <ion-card-content>\n            <h2 class="d-flex">Show Triffic on Map\n                <ion-checkbox color="secondary" checked="true"></ion-checkbox>\n            </h2>\n        </ion-card-content>\n    </ion-card>\n    <p padding class="text-light"><span padding-left padding-right>Other</span></p>\n    <ion-card padding>\n        <ion-card-content>\n            <h2 class="d-flex">Account Status\n                <ion-toggle checked="true" color="secondary"></ion-toggle>\n            </h2>\n        </ion-card-content>\n    </ion-card>\n    <ion-card padding>\n        <ion-card-content>\n            <h2>Contacts</h2>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\settings\settings.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], SettingsPage);
    return SettingsPage;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SharePage = /** @class */ (function () {
    function SharePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    SharePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-share',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\share\share.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            Share\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="bg-light">\n    <p padding class="text-light"><span padding-left padding-right>Share With</span></p>\n    <ion-card padding>\n        <ion-card-content>\n            <h2>Facebook</h2>\n        </ion-card-content>\n    </ion-card>\n    <ion-card padding>\n        <ion-card-content>\n            <h2>Whatsapp</h2>\n        </ion-card-content>\n    </ion-card>\n    <ion-card padding>\n        <ion-card-content>\n            <h2>Twitter</h2>\n        </ion-card-content>\n    </ion-card>\n    <ion-card padding>\n        <ion-card-content>\n            <h2>Google+</h2>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\share\share.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], SharePage);
    return SharePage;
}());

//# sourceMappingURL=share.js.map

/***/ }),

/***/ 53:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    LoginPage.prototype.tabs = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    LoginPage.prototype.signupPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\login\login.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>{{\'sign_in\' | translate}}</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding-left padding-right padding-top text-center>\n    <img src="assets/imgs/logo.png" class="logo">\n    <div padding>\n        <ion-list class="form" no-lines>\n            <ion-item class="bg-light">\n                <ion-label>{{\'phone_number\' | translate}}</ion-label>\n                <ion-input type="text" value="+91 9876543210" text-end></ion-input>\n            </ion-item>\n            <ion-item class="bg-light">\n                <ion-label>{{\'password\' | translate}}</ion-label>\n                <ion-input class="password" type="password" value="*******" text-end></ion-input>\n            </ion-item>\n        </ion-list>\n        <button ion-button block full text-uppercase class="bg-green text-white btn round shadow-green" (click)="tabs()">{{\'login\' | translate}}</button>\n    </div>\n\n    <ion-row padding-left padding-right>\n        <ion-col (click)="signupPage()"><small>{{\'new_user\' | translate}}<strong class="text-theme">{{\'sign_up\' | translate}}</strong></small></ion-col>\n        <ion-col text-end><small>{{\'forgot\' | translate}}<strong class="text-theme">{{\'password?\' | translate}} </strong></small></ion-col>\n    </ion-row>\n</ion-content>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__earnings_earnings__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ratings_ratings__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(214);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__ratings_ratings__["a" /* RatingsPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_1__earnings_earnings__["a" /* EarningsPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\tabs\tabs.html"*/'<ion-tabs>\n    <ion-tab [root]="tab1Root" tabTitle="{{\'home\' | translate}}" tabIcon="md-car" tabsHideOnSubPages="true"></ion-tab>\n    <ion-tab [root]="tab2Root" tabTitle="{{\'rating\' | translate}}" tabIcon="md-star" tabsHideOnSubPages="true"></ion-tab>\n    <ion-tab [root]="tab3Root" tabTitle="{{\'earning\' | translate}}" tabIcon="md-cash" tabsHideOnSubPages="true"></ion-tab>\n    <ion-tab [root]="tab4Root" tabTitle="{{\'profile\' | translate}}" tabIcon="md-person" tabsHideOnSubPages="true"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RiderinfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { CancelridePage } from '../cancelride/cancelride';
var RiderinfoPage = /** @class */ (function () {
    function RiderinfoPage(navCtrl, modalCtrl, viewCtrl) {
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
        this.viewCtrl = viewCtrl;
    }
    RiderinfoPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    RiderinfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-riderinfo',template:/*ion-inline-start:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\riderinfo\riderinfo.html"*/'<ion-header class="bg-transparent">\n    <ion-navbar>\n        <ion-title>\n            <ion-icon name="md-close" (click)="dismiss()" class="end"></ion-icon>\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="modal_container" text-center>\n        <div class="rider_img">\n            <img src="assets/imgs/2.png">\n        </div>\n        <div class="rider_details">\n            <h2>Sam Smith</h2>\n            <h3 class="text-theme">+19 987 654 362</h3>\n        </div>\n        <p class="d-flex"><span>4.8</span>\n            <ion-icon name="md-star" class="active"></ion-icon>\n            <ion-icon name="md-star" class="active"></ion-icon>\n            <ion-icon name="md-star" class="active"></ion-icon>\n            <ion-icon name="md-star" class="active"></ion-icon>\n            <ion-icon name="md-star" class=""></ion-icon>\n        </p>\n    </div>\n</ion-content>\n<ion-footer no-border>\n    <button ion-button block class="shadow-green btn">Call Sam</button>\n</ion-footer>\n'/*ion-inline-end:"C:\Users\user\Desktop\Cabber\Cabber_Driver\src\pages\riderinfo\riderinfo.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ViewController */]])
    ], RiderinfoPage);
    return RiderinfoPage;
}());

//# sourceMappingURL=riderinfo.js.map

/***/ })

},[224]);
//# sourceMappingURL=main.js.map