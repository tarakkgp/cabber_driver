import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { LoginPage } from '../login/login';
@Component({
  selector: 'page-myprofile',
  templateUrl: 'myprofile.html'
})
export class MyprofilePage {

  constructor(public navCtrl: NavController) {

  }  
  login(){
     this.navCtrl.setRoot(LoginPage);
    }
}
