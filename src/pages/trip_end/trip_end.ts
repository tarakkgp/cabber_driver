import { Component, ViewChild, ElementRef } from '@angular/core';
import { ModalController, NavController, ViewController } from 'ionic-angular';
import { FarePage } from '../fare/fare';
import { RiderinfoPage } from '../riderinfo/riderinfo';

declare var google;

@Component({
	selector: 'page-trip_end',
	templateUrl: 'trip_end.html'
})

export class Trip_endPage {
	@ViewChild('map') mapElement: ElementRef;
	map: any;

	constructor(public navCtrl: NavController, public modalCtrl: ModalController, public viewCtrl: ViewController) {

	}

	ionViewDidLoad() {
		let latLng = new google.maps.LatLng(20.5937, 78.9629);
		this.loadMap(latLng);
	}

	loadMap(latLng) {
		let mapOptions = {
			center: latLng,
			zoom: 12,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
	}

	//	acceptriderPage() {
	//		let modal = this.modalCtrl.create(AcceptriderPage);
	//		modal.present();
	//    }

	riderinfo() {
		let modal = this.modalCtrl.create(RiderinfoPage);
		modal.present();
	}
	fare() {
		let modal = this.modalCtrl.create(FarePage);
		modal.present();
	}
	//   location(){
	//     this.navCtrl.push(LocationPage);
	//     }


}
