import { NavController, ModalController } from 'ionic-angular';
import { Component, ViewChild, ElementRef } from '@angular/core';

declare var google;
import { RiderinfoPage } from '../riderinfo/riderinfo';
import { Trip_endPage } from '../trip_end/trip_end';

@Component({
	selector: 'page-location',
	templateUrl: 'location.html'
})
export class LocationPage {
	@ViewChild('map') mapElement: ElementRef;
	map: any;

	constructor(public navCtrl: NavController, public modalCtrl: ModalController) {

	}

	ionViewDidLoad() {
		let latLng = new google.maps.LatLng(20.5937, 78.9629);
		this.loadMap(latLng);
	}

	loadMap(latLng) {
		let mapOptions = {
			center: latLng,
			zoom: 12,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
	}

	trip_end() {
		this.navCtrl.push(Trip_endPage);
	}

	riderinfo() {
		let modal = this.modalCtrl.create(RiderinfoPage);
		modal.present();
	}

}
