import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
@Component({
  selector: 'page-fare',
  templateUrl: 'fare.html'
})
export class FarePage {

  constructor(public navCtrl: NavController) {

  }
 tabs(){
    this.navCtrl.setRoot(TabsPage);
    }
}