import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { TabsPage } from '../tabs/tabs';
import { SignupPage } from '../signup/signup';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  constructor(public navCtrl: NavController) {
  }
  
    tabs(){
     this.navCtrl.setRoot(TabsPage);
    }
    signupPage(){
    this.navCtrl.push(SignupPage);
    }

}  