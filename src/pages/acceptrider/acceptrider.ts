import { NavController } from 'ionic-angular';
import { FarePage } from '../fare/fare';
import { Component, ViewChild, ElementRef } from '@angular/core';

declare var google;

@Component({
	selector: 'page-acceptrider',
	templateUrl: 'acceptrider.html'
})

export class AcceptriderPage {
	@ViewChild('map') mapElement: ElementRef;
	map: any;

	constructor(public navCtrl: NavController) {

	}

	farePage() {
		this.navCtrl.push(FarePage);
	}

	ionViewDidLoad() {
		let latLng = new google.maps.LatLng(20.5937, 78.9629);
		this.loadMap(latLng);
	}

	loadMap(latLng) {
		let mapOptions = {
			center: latLng,
			zoom: 12,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
	}

}
