import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {MytripsPage } from '../mytrips/mytrips';
@Component({
  selector: 'page-ratings',
  templateUrl: 'ratings.html'
})
export class RatingsPage {

  constructor(public navCtrl: NavController) {

  }

 mytrips(){
    this.navCtrl.push(MytripsPage);
    }
}
