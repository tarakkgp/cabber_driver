import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AddmoneyPage } from '../addmoney/addmoney';
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html'
})
export class WalletPage {
  constructor(public navCtrl: NavController) {

  }
   
addmoney(){
    this.navCtrl.push(AddmoneyPage);
    }

}
